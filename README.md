Ansible Role: Django
=========

Setup environment for django application with gunicorn

Requirements
------------

No special requirements

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

None

Example Playbook
----------------

    - name: Setup django
      hosts: app
      become: true
      gather_facts: true
      roles:
        - ansible_role_django

License
-------

MIT

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
